$(document).ready(function(){
    $('.modal').modal();
    $('ul.tabs').tabs({

    });
    localStorage.removeItem('delOrderId');
    localStorage.removeItem('selectedDeliveryMedium');
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['viewType'] = "NEW"
    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/orderlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var dataUse = JSON.parse(jsonData);
        var data = dataUse.Data;
        var print = ""
        var totalCount = dataUse.totalCount;
        if(totalCount>0){
          $.each(data, function(i,v){
            print = print+"<div class=card horizontal><div class=card-stacked><div class=card-content><span class=card-title>Order Id: "+v.orderId+"</span><p><ul class=collection><li class=collection-item>Customer Name: "+v.custName+"</li><li class=collection-item>Created On: "+v.creationDate+"</li><li class=collection-item>Total amoun : "+v.amount+"</li><li class=collection-item>Order status: "+v.orderStatus+"</li><li class=collection-item>Payment Method : "+v.paymentMethod+"</li></ul></p></div><div class=card-action><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderId+">View Details</a></div></div></div>";
          })
        }
        else{
          print = "<div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>Card Title</span><p>Sorry currenty no orders in this section.</p></div></div>";
        }
        $('#test-swipe-1').append(print);
      }
    });
    myObj['viewType'] = "INPROGRESS"
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/orderlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var dataUse = JSON.parse(jsonData);
        var data = dataUse.Data;
        var print = ""
        var totalCount = dataUse.totalCount;
        if(totalCount>0){
          $.each(data, function(i,v){
            print = print+"<div class=card horizontal><div class=card-stacked><div class=card-content><span class=card-title>Order Id: "+v.orderId+"</span><p><ul class=collection><li class=collection-item>Customer Name: "+v.custName+"</li><li class=collection-item>Created On: "+v.creationDate+"</li><li class=collection-item>Total amoun : "+v.amount+"</li><li class=collection-item>Order status: "+v.orderStatus+"</li><li class=collection-item>Payment Method : "+v.paymentMethod+"</li></ul></p></div><div class=card-action><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderId+">View Details</a></div></div></div>";
          })
        }
        else{
          print = "<div class=row><div class=col s12 m6><div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>Card Title</span><p>Sorry currenty no orders in this section.</p></div></div></div></div>";
        }
        $('#test-swipe-2').append(print);
      }
    });
    myObj['viewType'] = "COMPLETED";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/orderlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var dataUse = JSON.parse(jsonData);
        var data = dataUse.Data;
        var print = ""
        var totalCount = dataUse.totalCount;
        if(totalCount>0){
          $.each(data, function(i,v){
            print = print+"<div class=card horizontal><div class=card-stacked><div class=card-content><span class=card-title>Order Id: "+v.orderId+"</span><p><ul class=collection><li class=collection-item>Customer Name: "+v.custName+"</li><li class=collection-item>Created On: "+v.creationDate+"</li><li class=collection-item>Total amoun : "+v.amount+"</li><li class=collection-item>Order status: "+v.orderStatus+"</li><li class=collection-item>Payment Method : "+v.paymentMethod+"</li></ul></p></div><div class=card-action><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderId+">View Details</a></div></div></div>";
          })
        }
        else{
          print = "<div class=row><div class=col s12 m6><div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>Card Title</span><p>Sorry currenty no orders in this section.</p></div></div></div></div>";
        }
        $('#test-swipe-3').append(print);
      }
    });
  });
