$(document).ready(function(){
    if(localStorage.getItem('sessionInfo')){
      var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
      var orderId = getUrlVars()["orderId"];
      var myObj = {};
      myObj['orderId']=orderId;
      myObj['typeId']=sessionInfo.typeId;
      myObj['userId']=sessionInfo.userId;
      myObj['userType']=sessionInfo.userType;
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/orderdetails',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          var jsonData = JSON.stringify(response);
          var dataUse = JSON.parse(jsonData);
          $('#orderId').append(dataUse.orderId);
          $('#orderStatus').append(dataUse.orderStatus);
          $('#createdOnDate').append(dataUse.creationDate);
          $('#paymentMethod').append(dataUse.paymentMethod);
          $('#custName').append(dataUse.custName);
          var custCall = dataUse.custPhoneNumber + '<a style="text-align:center; vertcial-align:middle;" href="tel:'+dataUse.custPhoneNumber+'"> <i class="material-icons" style="text-align:center; vertcial-align:middle;">call</i></a>';
          $('#custNumber').append(custCall);
          $('#shpName').append(dataUse.shpName);
          var shopCall = dataUse.shpPhoneNumber + '<a style="text-align:center; vertcial-align:middle;" href="tel:'+dataUse.shpPhoneNumber+'"> <i class="material-icons" style="text-align:center; vertcial-align:middle;">call</i></a>';
          $('#shpNumber').append(shopCall);
          $('#addressName').append(dataUse.addName);
          var address = dataUse.addLine1+" "+dataUse.addLine2+" "+dataUse.city+" "+dataUse.state+" "+dataUse.country+" "+dataUse.pincode;
          $('#deliveryAddress').append(address);
          $('#totalAmount').append(dataUse.amount);
          $('#totalItems').append(dataUse.itemCount);
          $('#dmName').append(dataUse.dmName);
          $('#dmNumber').append(dataUse.dmPhone);
          var items = dataUse.items;
          $.each(items, function(i,v){
            var print = "<tr><td>"+v.itemName+"</td><td>"+v.amount+"</td><td>"+v.quantity+"</td></tr>";
            $('#itemsBody').append(print);
          })
        },
        error: function(response){
          Materialize.toast('Not authorized to view the page', 4000)
          setTimeout(function(){
             window.location.replace(feUrl()+"/index.html");
          }, 500);
        }
      });
    }
    else{
      Materialize.toast('Not authorized to view the page', 4000)
      setTimeout(function(){
         window.location.replace(feUrl()+"/index.html");
      }, 500);
    }
  });

  function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
    });
    return vars;
  }
