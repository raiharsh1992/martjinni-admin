$(document).ready(function(){
  if(localStorage.getItem('sessionInfo')){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/getcustlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        console.log(response);
        var custData = JSON.stringify(response);
        var custInfo = JSON.parse(custData);
        var totalCount = custInfo.totalCount;
        if(totalCount>0){
          var abc = custInfo.custList;
          var col = "";
          $.each(abc, function(i,v){
            var print = "<div class=card horizontal><div class=card-stacked><div class=card-content><span class=card-title>Customer Name: "+v.custName+"</span><p><ul class=collection><li class=collection-item>Customer Email: "+v.custEmail+"</li><li class=collection-item>Customer PhoneNumber: "+v.custPhoneNumber+"</li></ul></p></div></div></div>";
            $('#test3').prepend(print);
          });
        }
        else{
          $('#modal1').modal('open');
        }
      }
    })
  }
  else{
    window.location.replace(feUrl()+"/index.html");
  }
});
