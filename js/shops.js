var outLat = 0;
var outLng = 0;
$(document).ready(function(){
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
  var data = $("#search").serialize();
  var place = autocomplete.getPlace();
  outLat =  place.geometry.location.lat();
  outLng = place.geometry.location.lng();
  console.log(place);
  //geocodeShop();
});

  $('.modal').modal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  localStorage.removeItem("shopForProfileUpdate");
  $('select').material_select();
  var dummy = {};
  dummy['viewType'] = "shop";
  dummyJson = JSON.stringify(dummy);
  $.ajax({
    url : beUrl()+'/viewcateogary',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:dummyJson,
    success: function(response) {
      var jSresponse = JSON.stringify(response);
      var useJsResponse = JSON.parse(jSresponse);
      var data = useJsResponse.data;
      $.each(data, function(i,v){
        var jPrint = "<option value="+v.catId+">"+v.catName+"</option>";
        $('#addCat').append(jPrint);

      });
      $('select').material_select();
    }
  });
  if(localStorage.getItem('sessionInfo')){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    var currentTime = new Date();
    $.ajax({
      url : beUrl()+'/getshoplist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        console.log(response);
        var shopData = JSON.stringify(response);
        var shopInfo = JSON.parse(shopData);
        var totalCount = shopInfo.totalCount;
        if(totalCount>0){
          var abc = shopInfo.shopList;
          var col = "";
          $.each(abc, function(i,v){
            if(v.shopIsOpen==1){
              var print = "<div class='card horizontal col s12 m9 l5 xl5'><div class=card-stacked><div class=card-content><span class=card-title>Customer Name: "+v.shopName+"</span><p><ul class=collection><li class=collection-item>Description: "+v.shopDescription+"</li><li class=collection-item>Shop phone: "+v.shopPhoneNumber+"</li><li class=collection-item>Shop Email: "+v.shopEmail+"</li><li class=collection-item>Shop Address: "+v.shopAddress+"</li><li class=collection-item>Shop Delivery Radius: "+v.deliveryRadius+"</li><li class=collection-item>Added on : "+currentTime+"</li><li class=collection-item>Status: Open</li></ul></p></div><div class=card-action><a href="+feUrl()+"/items.html?shopId="+v.shopId+">View Items</a><a href=javascript:uploadProfile("+v.shopId+")>Upload Image</a><a href=javascript:setProperty("+v.shopId+")>Property</a><a href=javascript:setVerified("+v.shopId+")>Verified</a></div></div></div>";
              $('#test3').prepend(print);
            }else{
              var print = "<div class='card horizontal col s12 m9 l5 xl5'><div class=card-stacked><div class=card-content><span class=card-title>Customer Name: "+v.shopName+"</span><p><ul class=collection><li class=collection-item>Description: "+v.shopDescription+"</li><li class=collection-item>Shop phone: "+v.shopPhoneNumber+"</li><li class=collection-item>Shop Email: "+v.shopEmail+"</li><li class=collection-item>Shop Address: "+v.shopAddress+"</li><li class=collection-item>Shop Delivery Radius: "+v.deliveryRadius+"</li><li class=collection-item>Added on : "+currentTime+"</li><li class=collection-item>Status: Closed</li></ul></p></div><div class=card-action><a href="+feUrl()+"/items.html?shopId="+v.shopId+">View Items</a><a href=javascript:uploadProfile("+v.shopId+")>Upload Image</a><a href=javascript:setProperty("+v.shopId+")>Property</a><a href=javascript:setVerified("+v.shopId+")>Verified</a></div></div></div>";
              $('#test3').prepend(print);
            }
          });
        }
        else{
          $('#modal1').modal('open');
        }
      }
    })
  }
  else{
    window.location.replace(feUrl()+"/index.html");
  }
  $("#signup1").click(function(){
    var userNameId = document.getElementById("signupUsername")
    var userNameClass = userNameId.className;
    var userPassId = document.getElementById("signuppassword");
    var userPassClass = userPassId.className;
    if((userNameClass=="validate valid")&&(userPassClass=="validate valid")){
      var signUpObj = {}
      signUpObj['userNameLogin']=userNameId.value;
      signUpObj['password']=userPassId.value;
      var signUpJson = JSON.stringify(signUpObj);
      sessionStorage.setItem('signUpJson',signUpJson)
      if(sessionStorage.getItem('signUpJson')){
        Materialize.toast('Almost there', 4000);
        $('#signUpOnly1').modal('close');
        $('#signUpOnly2').modal('open');
      }
    }
    else {
      Materialize.toast('See the error message', 4000);
    }
  });
  $("#signup2").click(function(){
    event.preventDefault();
    var custNameId = document.getElementById('signupCustname');
    var custEmailId = document.getElementById('userEmail');
    var custPhoneId = document.getElementById('userPhone');
    var userAddressId = document.getElementById('userAddress');
    var descriptionId = document.getElementById('description');
    var delRadiusId = document.getElementById('delRadius');
    var delCharges = document.getElementById('delCharges');
    var minOrderValue = document.getElementById('minOrderValue');
    //var shopLocation = document.getElementById('addLocale').value;
    var shopType = document.getElementById('shopType').value;
    var custNameClass = custNameId.className;
    var custEmailClass = custEmailId.className;
    var custPhoneClass = custPhoneId.className;
    var userAddressClass = userAddressId.className;
    var descriptionClass = descriptionId.className;
    var delRadiusClass = delRadius.className;
    var delChargesClass = delCharges.className;
    var minOrderValueClass = minOrderValue.className;
    var shopCat = $('#addCat').val();
    var shopValue = "n"
    if(shopCat[0]){
      shopValue = shopCat.join(",");
    }
    if((custNameClass=="validate valid")&&(custEmailClass=="validate valid")&&(custPhoneClass=="validate valid")&&(userAddressClass=="validate valid")&&(descriptionClass=="validate valid")&&(delRadiusClass=="validate valid")&&(minOrderValueClass=="validate valid")&&(delChargesClass=="validate valid")&&(shopValue!="n")&&((shopType==5)||(shopType==6)||(shopType==7))&&(outLat!=0)&&(outLng!=0))
    {
      var loginObject = sessionStorage.getItem('signUpJson');
      var loginPars = JSON.parse(loginObject);
      sessionStorage.removeItem('signUpJson');
      var signUpObject = {};
      signUpObject['userType']="SHP";
      signUpObject['userNameLogin']=loginPars.userNameLogin;
      signUpObject['password']=loginPars.password;
      signUpObject['phoneNumber']=custPhoneId.value;
      signUpObject['custName']=custNameId.value;
      signUpObject['emailId']=custEmailId.value;
      signUpObject['address']=userAddressId.value;
      signUpObject['description'] = descriptionId.value;
      signUpObject['delRadius'] = delRadiusId.value;
      signUpObject['delCharges'] = delCharges.value;
      signUpObject['minOrderValue'] = minOrderValue.value;
      signUpObject['catId'] = shopValue;
      signUpObject['lat'] = outLat;
      signUpObject['lng'] = outLng;
      if(shopType==5){
        signUpObject['delType'] = 1;
      }
      else if(shopType==6){
        signUpObject['delType'] = 2;
      }
      else{
        signUpObject['delType'] = 3;
      }
      console.log(signUpObject);
      var jsonSignUpJson = JSON.stringify(signUpObject);
      sessionStorage.setItem('signUpJson',jsonSignUpJson);
      var otp = {};
      otp['phoneNumber']=custPhoneId.value;
      otp['userType']="SHP";
      otp['userNeed']="CREATE";
      var jsonOtp =JSON.stringify(otp);
      $.ajax({
        url : beUrl()+'/generateotp',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        data:jsonOtp,
        success: function(response) {
          Materialize.toast('Please see your phone for OTP', 4000);
          $('#signUpOnly2').modal('close');
          $('#signUpOnly3').modal('open');
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    }
    else if(outLat==0||outLng==0){
      Materialize.toast('Kindly select shop location', 4000);
    }
    else {
      Materialize.toast('See the error message', 4000);
    }
  });
  $("#signup3").click(function(){
    event.preventDefault();
    var otpId = document.getElementById('custOTP');
    var otpclass = otpId.className;
    if(otpclass=="validate valid"){
      var signUpObjectFinal = sessionStorage.getItem('signUpJson');
      var jsonParser = JSON.parse(signUpObjectFinal);
      var finalObject = {};
      finalObject['otp']=otpId.value;
      finalObject['userNameLogin']=jsonParser.userNameLogin;
      finalObject['password']=jsonParser.password;
      finalObject['phoneNumber']=jsonParser.phoneNumber;
      finalObject['shopName']=jsonParser.custName;
      finalObject['emailId']=jsonParser.emailId;
      finalObject['lat']=jsonParser.lat;
      finalObject['lng']=jsonParser.lng;
      finalObject['address']=jsonParser.address;
      finalObject['userType']="SHP";
      finalObject['typeId'] = sessionInfo.typeId;
      finalObject['description'] = jsonParser.description;
      finalObject['delRadius'] = jsonParser.delRadius;
      finalObject['delCharges'] = jsonParser.delCharges;
      finalObject['minOrderValue'] = jsonParser.minOrderValue;
      finalObject['catId'] = jsonParser.catId;
      finalObject['delType'] = jsonParser.delType;
      finalObject['delToken'] = 0;
      var finalJsonObject = JSON.stringify(finalObject);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/createuser',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:finalJsonObject,
        success: function(response) {
          Materialize.toast('Please wait for magic', 4000);
          setTimeout(function(){
            location.reload();
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    }
    else {
      Materialize.toast('Please pass OTP', 4000);
    }
    console.log(otpclass);
  })
});

function userAvailability() {
  var userData = document.getElementById('signupUsername').value;
  myObj = {};
  myObj['userName'] = userData;
  myObj['mode']="client";
  var jsonData = JSON.stringify(myObj);
  $.ajax({
    url : beUrl()+'/validateuser',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:jsonData,
    error: function(response) {
      $("#signupUsername").next("label").attr('data-error','The userName is already taken');
      $("#signupUsername").removeClass("valid");
      $("#signupUsername").addClass("invalid");
    },
    success: function (response) {
      $("#signupUsername").next("label").attr('data-success','The userName is available');
      $("#signupUsername").removeClass("invalid");
      $("#signupUsername").addClass("valid");
    }
  });
}

function passwordValidity() {
  var validatePass = document.getElementById('signuppassword');
  var length = validatePass.value.length;
  if(length<8){
    $("#signuppassword").next("label").attr('data-error','Too small password');
    $("#signuppassword").removeClass("valid");
    $("#signuppassword").addClass("invalid");
  }
  else if (length>24) {
    $("#signuppassword").next("label").attr('data-error','Too big password');
    $("#signuppassword").removeClass("valid");
    $("#signuppassword").addClass("invalid");
  }
  else {
    $("#signuppassword").next("label").attr('data-success','The password is acceptable');
    $("#signuppassword").removeClass("invalid");
    $("#signuppassword").addClass("valid");
  }
}
function phoneAvailablity() {
  var validatePhone = document.getElementById('userPhone');
  var length = validatePhone.value.length;
  if(length<10){
    $("#userPhone").next("label").attr('data-error','Too small phone number');
    $("#userPhone").removeClass("valid");
    $("#userPhone").addClass("invalid");
  }
  else if (length>10) {
    $("#userPhone").next("label").attr('data-error','Too big phone number');
    $("#userPhone").removeClass("valid");
    $("#userPhone").addClass("invalid");
  }
  else {
    var phoneNumber = validatePhone.value;
    var phoneObj = {};
    phoneObj['phoneNumber']=phoneNumber;
    phoneObj['userType']="SHP";
    var jsonPhone = JSON.stringify(phoneObj);
    $.ajax({
      url : beUrl()+'/validphone',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:jsonPhone,
      error: function(response) {
        $("#userPhone").next("label").attr('data-error','The phoneNumber is already taken');
        $("#userPhone").removeClass("valid");
        $("#userPhone").addClass("invalid");
      },
      success: function (response) {
        $("#userPhone").next("label").attr('data-success','The phoneNumber is available');
        $("#userPhone").removeClass("invalid");
        $("#userPhone").addClass("valid");
      }
    });
  }
}

function addShop() {
  $('#signUpOnly1').modal('open');
}

function uploadProfile(shopId) {
  localStorage.setItem("shopForProfileUpdate", shopId);
  $('#uploadImage').modal('open');
}

function finalUpload() {
  if((localStorage.getItem("shopForProfileUpdate"))&&(localStorage.getItem("sessionInfo"))){
    var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
    var shopId = localStorage.getItem("shopForProfileUpdate");
    var form = new FormData();
    var head = sessionInfo.basicAuthenticate;
    form.append("userId", sessionInfo.userId);
    form.append("typeId", sessionInfo.typeId);
    form.append("userType", sessionInfo.userType);
    form.append("proPic", $( '#file' )[0].files[0]);
    form.append("shopId", shopId);
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": beUrl()+"/uploadprofileshop",
      "method": "POST",
      "headers": {
      "basicauthenticate": ""+head,
      },
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "data": form
    }
    $.ajax(settings).done(function (response) {
      Materialize.toast('Please wait for magic', 4000);
      setTimeout(function(){
        location.reload();
      }, 1000);
    });
  }else{
    window.location.reload();
  }
}

function setProperty(shopId){
  localStorage.setItem('shopForProperty',shopId);
  var shopSend={};
  shopSend['shopId']= shopId;
  var data=JSON.stringify(shopSend);
  $.ajax({
    url : beUrl()+'/getshopproperty',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:data,
    error: function(response) {
      console.log('gandh maccha diyea');
    },
    success: function (response) {
      var d =  JSON.stringify(response);
      var d1 = JSON.parse(d);
      var data = d1.data;
      $.each(data, function(i,v){
        var propertyName=v.propertyName;
        var propertyId=v.propertyId;
        var propertyValue =v.propertyValue;
        var propertyNameUse = "n"+propertyId
        localStorage.setItem(propertyNameUse,propertyName);
        var print = "<option value="+propertyId+" id="+propertyNameUse+">"+propertyName+"</option>";
        $('#slecetProperty').append(print);
        $('select').material_select();
      });
      $('#setProperty').modal('open');
    }
  });
}
function setFinalProperty() {
  var propertyId = document.getElementById("slecetProperty").value;
  var propertyNameUse = "n"+propertyId
  var propertyName=localStorage.getItem(propertyNameUse);
  var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
  var myObj = {};
  myObj['userId'] = sessionInfo.userId;
  myObj['typeId'] = sessionInfo.typeId;
  myObj['userType'] = sessionInfo.userType;
  myObj['propertyName'] = propertyName;
  myObj['propertyId'] = propertyId;
  myObj['propertyValue'] = document.getElementById("propertyValue").value;
  myObj['shopId'] = localStorage.getItem('shopForProperty');
  myObj['forUserType'] = "SHP";

  var head = sessionInfo.basicAuthenticate;
  var data=JSON.stringify(myObj);
  console.log(data);
  $.ajax({
    url : beUrl()+'/clientshopproperty',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:data,
    headers : {"basicauthenticate":""+head},
    error: function(response) {
      console.log('gandh maccha diyea');
    },
    success: function (response) {
      window.location.reload();
    }
  });

}

function setVerified(shopId){
  var myObj={};
  var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
  myObj['userId'] = sessionInfo.userId;
  myObj['typeId'] = sessionInfo.typeId;
  myObj['userType'] = sessionInfo.userType;
  myObj['isActive'] = 1;
  myObj['shopId'] = shopId;
  var head = sessionInfo.basicAuthenticate;
  var data=JSON.stringify(myObj);
  console.log(data);
  $.ajax({
    url : beUrl()+'/storeactive',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:data,
    headers : {"basicauthenticate":""+head},
    error: function(response) {
      console.log('gandh maccha diyea');
    },
    success: function (response) {
      window.location.reload();
    }
  });
}
function resendotp()
{
  console.log('OTP resent');
  var phoneNumber=JSON.parse(sessionStorage.getItem('signUpJson')).phoneNumber;
  console.log(phoneNumber);
  var otp = {};
  otp['phoneNumber']=phoneNumber;
  otp['userType']="CUST";
  otp['userNeed']="SIGNUP";
  var jsonOtp =JSON.stringify(otp);
  $.ajax({
    url : beUrl()+'/generateotp',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:jsonOtp,
    success: function(response) {
      Materialize.toast('OTP has been resent', 4000);
      $('#signUpOnly3').modal('open');
    },
    error: function (response) {
      var responseJson = JSON.stringify(response);
      var responseJsonR = JSON.parse(responseJson);
      var statusCode = responseJsonR.responseJSON;
      var data = JSON.stringify(statusCode);
      var xData = JSON.parse(data);
      var message = xData.Data;
      Materialize.toast(message, 4000);
    }
  });
}
function geocodeShop() {
  console.log("WOW");
  var userPassed = document.getElementById('search').value;
  var dummy = userPassed;
  userPassed=userPassed.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g,"");   //this one
  userPassed=userPassed.replace(/\s+/g, "+");
  var url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+userPassed+'&key=AIzaSyAQF1IHXvQpG4nEP1wQA1n-9eHcbnk3w3w';
  $.ajax({
      url : url,
      type: 'GET',
      dataType:'json',
      processData: false,
      complete: function(data) {
          var d =  JSON.stringify(data);
          var d1 = JSON.parse(d);
          var abc = d1.responseJSON.results;
          $.each(abc,function(i,v){
              outLat = v.geometry.location.lat;
              outLng = v.geometry.location.lng;
          });
      },
      error: function() {
           Materialize.toast('Please pass an address', 4000)
      },
  });
}
