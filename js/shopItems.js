$(document).ready(function(){
  var shopId = getUrlVars()["shopId"];
  var myObj = {};
  myObj['shopId']=shopId;
  var data = JSON.stringify(myObj);
  $.ajax({
    url : beUrl()+'/itemlist',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:data,
    success: function(response) {
      var storeItem = JSON.stringify(response);
      var storeItemData = JSON.parse(storeItem);
      var totalCount = storeItemData.totalCount;
      if(totalCount>0){
        var itemData = storeItemData.data;
        var x = 0;
        $.each(itemData, function(i,v){
          var mainItemId = v.mainItemId;
          var mainItemName = v.Name;
          var subItems = v.subItems;
          var print = "<div class=collapsible-header><i class=material-icons>local_dining</i>"+mainItemName+"</div>"
          var xPrint = "";
          $.each(subItems, function(k, l) {
              var subItemName = l.Name;
              var subItemId = l.subItemId;
              var rate = l.rate;
              xPrint = xPrint+"<tr><td>"+subItemName+"</td><td>"+rate+"</td></tr>";
          });
          var jPrint = "<div class=collapsible-body><span><table><thead><tr><th>Name</th><th>Price</th></tr></thead><tbody>"+xPrint+"</tbody></table></span></div>"
          print = "<li>"+print+jPrint+"</li>"
          $('#itemList').append(print);
        });
        localStorage.setItem('storeId',shopId);
      }
      else{
        var $toastContent = $('<span>No items added by the store</span>').add($('<button class="btn-flat toast-action" onclick="redirect()">Back</button>'));
        Materialize.toast($toastContent);
      }
    }
  });
});

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
  });
  return vars;
}

function redirect(){
  window.location.replace(feUrl()+"/shops.html");
}

function emptyCart(){
  localStorage.removeItem('cartCount');
  localStorage.removeItem('cartStoreId');
  Materialize.Toast.removeAll();
  var itemCount = localStorage.getItem('itemCount');
  for (i=0; i<itemCount;i++){
    var qx = 'q'+i;
    if(localStorage.getItem(qx)){
      localStorage.removeItem(qx)
    }
  }
}

function mart(){
  window.location.replace(feUrl()+"/cart.html");
}

function addItem(x){
  Materialize.Toast.removeAll();
  document.getElementById(x).stepUp(1);
  var qx = 'q'+x;
  if(localStorage.getItem(qx)){
    var quantity = localStorage.getItem(qx)
    quantity ++;
    localStorage.setItem(qx, quantity)
    var $toastContent = $('<span>Items added</span>').add($('<button class="btn-flat toast-action" onclick="mart()">Proceed</button>'));
    Materialize.toast($toastContent);
  }
  else {
    localStorage.setItem(qx, 1);
    if(localStorage.getItem('cartCount')){
      var cartCount = localStorage.getItem('cartCount');
      cartCount ++;
      localStorage.setItem('cartCount',cartCount);
      var $toastContent = $('<span>Items added</span>').add($('<button class="btn-flat toast-action" onclick="mart()">Proceed</button>'));
      Materialize.toast($toastContent);
    }
    else {
      localStorage.setItem('cartCount',1);
      var storeId = localStorage.getItem('storeId');
      localStorage.setItem('cartStoreId',storeId);
      var $toastContent = $('<span>Items added</span>').add($('<button class="btn-flat toast-action" onclick="mart()">Proceed</button>'));
      Materialize.toast($toastContent);
    }
  }
}

function substractItem(x){
  var jhol = document.getElementById(x).value;
  if(jhol>=1)
  {
    document.getElementById(x).stepDown(1);
    var qx = 'q'+x;
    if(localStorage.getItem(qx)){
      var quantity = localStorage.getItem(qx)
      quantity --;
      localStorage.setItem(qx, quantity);
      if(quantity==0)
      {
        var cartCount = localStorage.getItem('cartCount');
        cartCount --;
        if(cartCount==0){
          emptyCart();
        }
        else {
          localStorage.setItem('cartCount',cartCount);
        }
      }
    }
  }
  else {
    Materialize.toast('Add item first', 4000)
  }
}
