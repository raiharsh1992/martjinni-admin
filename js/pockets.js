$(document).ready(function(){
     $('.modal').modal();
  $.ajax({
    url : beUrl()+'/viewpocket',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    success: function(response) {
        var json = JSON.stringify(response);
        var son = JSON.parse(json);
        var data = son.Data;
        $.each(data, function(i,v){
            var pocketId = v.pocketId;
            var pocketName = v.pocketName;
            var city = v.city;
            var state = v.state;
            var country = v.country;
            var pincode = v.pincode;
            var lat = v.lat;
            var lng = v.lng;
            var keywords = v.keywords;
            var isActiveFl = v.isActiveFl;
            if(isActiveFl==1){
                var hello = "true"
            }
            else{
                var hello = "false"
            }
            var printx = "<tr><td>"+pocketId+"</td><td>"+pocketName+"</td><td>"+city+"</td><td>"+state+"</td><td>"+country+"</td><td>"+pincode+"</td><td>"+lat+"</td><td>"+lng+"</td><td>"+keywords+"</td><td>"+hello+"</td></tr>";
            $("#info").append(printx);
        });
    },
      error: function(response){
          var res = JSON.stringify(response);
          resparsed = JSON.parse(res);
          var data = resparsed.DATA;
          console.log(data);
      }
  });
});
function create(){
    $('#contactModal').modal('open');
}
function createPocket(){
    $('#contactModal').modal('open');
    var sess = localStorage.getItem("sessionInfo");
    var sessionInfo = JSON.parse(sess);
    var typeId= sessionInfo.typeId;
    var userId = sessionInfo.userId;
    var userType = sessionInfo.userType;
    var pocketName = $("#pocketName").val();
    var pocketCity = $("#pocketCity").val();
    var pocketState = $("#pocketState").val();
    var pocketCountry = $("#pocketCountry").val();
    var pocketPin = $("#pocketPin").val();
    var pocketKey = $("#pocketKey").val();
    var pocketLat = $("#pocketLat").val();
    var pocketLng = $("#pocketLng").val();
    var radius = $("#radius").val();
    var head = sessionInfo.basicAuthenticate;
    var jsonObj = {};
    jsonObj['typeId']=typeId;
    jsonObj['userId']=userId;
    jsonObj['userType']=userType;
    jsonObj['pocketName']=pocketName;
    jsonObj['pocketCity']=pocketCity;
    jsonObj['pocketState']=pocketState;
    jsonObj['pocketCountry']=pocketCountry;
    jsonObj['pocketPin']=pocketPin;
    jsonObj['pocketKey']=pocketKey;
    jsonObj['pocketLat']=pocketLat;
    jsonObj['pocketLng']=pocketLng;
    jsonObj['radius']=radius;
    var myObj = JSON.stringify(jsonObj);
    console.log(myObj);
      $.ajax({
   url : beUrl()+'/addpocket',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:myObj,
    success: function(response) {
        var json = JSON.stringify(response);
        var son = JSON.parse(json);
        var data = son.Data;
        console.log(data);
        location.reload();
    },
      error: function(response){
          var res = JSON.stringify(response);
          resparsed = JSON.parse(res);
          var data = resparsed.DATA;
    
      }
  });
    
}