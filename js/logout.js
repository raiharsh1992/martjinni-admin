$(document).ready(function(){
  $('.modal').modal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  if(localStorage.getItem('sessionInfo')){
    $('#logout').modal('open');
  }
  else{
    Materialize.toast('Not authorized to view the page', 4000)
    setTimeout(function(){
       window.location.replace(feUrl()+"/index.html");
    }, 500);
  }
})

function logoutModal(){
  removeToken();
}

function signOut() {
  if(localStorage.getItem('sessionInfo')){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId']=sessionInfo.userId;
    myObj['userType']=sessionInfo.userType;
    var data = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/signout',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:data,
      success: function(response) {
          Materialize.toast('Successfully logged out', 4000);
          localStorage.removeItem('chosenAddId');
          localStorage.removeItem('delVal');
          localStorage.removeItem('storeId');
          localStorage.removeItem('sessionInfo');
          window.location.replace(feUrl()+"/index.html");
      },
      error: function(response){
        Materialize.toast('Not authorized to view the page', 4000);
        localStorage.removeItem('sessionInfo');
        setTimeout(function(){
           window.location.replace(feUrl()+"/index.html");
        }, 500);
      }
    });
  }
}

function removeToken() {
  var config = {
  apiKey: "AIzaSyDVvGpg-qatublDZeflhaCZ--RNj5qKFdk",
  authDomain: "usermartjinni.firebaseapp.com",
  databaseURL: "https://usermartjinni.firebaseio.com",
  projectId: "usermartjinni",
  storageBucket: "usermartjinni.appspot.com",
  messagingSenderId: "899688528611"
};
  if(sessionStorage.getItem("newToken")){
    if (localStorage.getItem('sessionInfo')) {
      var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        var head = sessionInfo.basicAuthenticate;
        var myObjUse = {};
        myObjUse['userId'] = sessionInfo.userId;
        myObjUse['userType'] = sessionInfo.userType;
        myObjUse['typeId'] = sessionInfo.typeId;
        myObjUse['token'] = sessionStorage.getItem("newToken");
        var finalObject = JSON.stringify(myObjUse);
        $.ajax({
            url: beUrl() + '/remusertoken',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            headers: {
                "basicauthenticate": "" + head
            },
            data: finalObject,
            success: function(response) {
              signOut();
            },
            error: function(response){
              signOut();
            }

        });
    }
  }
  else{
    console.log(userToken);
    if (localStorage.getItem('sessionInfo')) {
      var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        var head = sessionInfo.basicAuthenticate;
        var myObjUse = {};
        myObjUse['userId'] = sessionInfo.userId;
        myObjUse['userType'] = sessionInfo.userType;
        myObjUse['typeId'] = sessionInfo.typeId;
        myObjUse['token'] = userToken;

        var finalObject = JSON.stringify(myObjUse);
        $.ajax({
            url: beUrl() + '/remusertoken',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            headers: {
                "basicauthenticate": "" + head
            },
            data: finalObject,
            success: function(response) {
              signOut();
            },
            error: function(response){
              signOut();
            }

        });
    }
  }
}
