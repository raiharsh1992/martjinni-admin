$(document).ready(function(){
  $('.modal').modal({
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  localStorage.removeItem("insertType");
  if(localStorage.getItem("sessionInfo")){
    var myObj = {};
    myObj['viewType']="shop";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/viewcateogary',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:jsonObj,
      success: function(response) {
        var json = JSON.stringify(response);
        var useJson = JSON.parse(json);
        var itemData = useJson.data;
        $.each(itemData, function(i,v){
          var print = "<tr><td>"+v.catName+"</td></tr>";
          var print2 = "<option value="+v.catId+">"+v.catName+"</option>";
          $('#shopInfo').append(print);
          $('#catOptions').append(print2);
        });
      }
    });
    var myObj = {};
    myObj['viewType']="items";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/viewcateogary',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:jsonObj,
      success: function(response) {
        var json = JSON.stringify(response);
        var useJson = JSON.parse(json);
        var itemData = useJson.data;
        $.each(itemData, function(i,v){
          var print = "<tr><td>"+v.catName+"</td></tr>";
          $('#itemsInfo').append(print);
        });
      }
    });
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
  var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
  var myObj = {};
  myObj['userId'] = sessionInfo.userId;
  myObj['typeId'] = sessionInfo.typeId;
  myObj['userType'] = sessionInfo.userType;
  var head = sessionInfo.basicAuthenticate;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url : beUrl()+'/viewmjcat.',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    headers : {"basicauthenticate":""+head},
    data:jsonObj,
    success: function(response) {
      var json = JSON.stringify(response);
      var useJson = JSON.parse(json);
      var itemData = useJson.Data;
      $.each(itemData, function(i,v){
        var print = "<tr><td>"+v.mjName+"</td></tr>";
        var optionMj = "<option value="+v.mjId+">"+v.mjName+"</option>"
        console.log(optionMj);
        $('#mjInfo').append(print);
        $('#mjOptions').append(optionMj);

      });
      $('select').material_select();
    },
    error: function (response) {
      console.log(response + "console.error();");
      console.error(response);
    }
  });
});

function insertNewItems() {
  if(localStorage.getItem("sessionInfo")){
    localStorage.setItem("insertType", "items");
    $('#insertNewitem').modal('open');
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
}

function insertNewShop() {
  if(localStorage.getItem("sessionInfo")){
    localStorage.setItem("insertType", "shop");
    $('#insertNew').modal('open');
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
}

function create() {
  if((localStorage.getItem('insertType'))&&(localStorage.getItem('sessionInfo'))){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    myObj['insertType'] = 'shop';
    myObj['catName'] = document.getElementById('catName').value;
    myObj['catKeyword'] = document.getElementById('catKeyword').value;
    myObj['mjId'] = document.getElementById('mjOptions').value;
    var head = sessionInfo.basicAuthenticate;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/insertcateogary.',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        Materialize.toast('Please wait for magic', 4000);
        setTimeout(function(){
          location.reload();
        }, 1000);
      },
      error: function (response) {
        var responseJson = JSON.stringify(response);
        var responseJsonR = JSON.parse(responseJson);
        var statusCode = responseJsonR.responseJSON;
        var data = JSON.stringify(statusCode);
        var xData = JSON.parse(data);
        var message = xData.Data;
        Materialize.toast(message, 4000);
      }
    });
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
}
function itemcreate() {
  if((localStorage.getItem('insertType'))&&(localStorage.getItem('sessionInfo'))){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    myObj['insertType'] = 'items';
    myObj['catName'] = document.getElementById('catNamex').value;
    myObj['catKeyword'] = document.getElementById('catKeywordx').value;
    myObj['shopCatId'] = document.getElementById('catOptions').value;
    var head = sessionInfo.basicAuthenticate;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/insertcateogary.',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        Materialize.toast('Please wait for magic', 4000);
        setTimeout(function(){
          location.reload();
        }, 1000);
      },
      error: function (response) {
        var responseJson = JSON.stringify(response);
        var responseJsonR = JSON.parse(responseJson);
        var statusCode = responseJsonR.responseJSON;
        var data = JSON.stringify(statusCode);
        var xData = JSON.parse(data);
        var message = xData.Data;
        Materialize.toast(message, 4000);
      }
    });
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
}

function insertMj(){
  if(localStorage.getItem('sessionInfo')){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    myObj['insertType'] = 'mjAvailable';
    myObj['mjName'] = document.getElementById('mjName').value;
    var head = sessionInfo.basicAuthenticate;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/insertcateogary.',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        Materialize.toast('Please wait for magic', 4000);
        setTimeout(function(){
          location.reload();
        }, 1000);
      },
      error: function (response) {
        var responseJson = JSON.stringify(response);
        var responseJsonR = JSON.parse(responseJson);
        var statusCode = responseJsonR.responseJSON;
        var data = JSON.stringify(statusCode);
        var xData = JSON.parse(data);
        var message = xData.Data;
        Materialize.toast(message, 4000);
      }
    });
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
}
function insertNewMj() {
  if(localStorage.getItem("sessionInfo")){
    localStorage.setItem("insertType", "items");
    $('#insertNewmj').modal('open');
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
}
