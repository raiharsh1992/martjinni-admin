$(document).ready(function(){
  $('.modal').modal({
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  if(localStorage.getItem("sessionInfo")){
    var sessionInfo = localStorage.getItem("sessionInfo");
    localStorage.clear();
    localStorage.setItem("sessionInfo",sessionInfo);
    $.ajax({
      url : beUrl()+'/getgstratelist',
      type: 'POST',
      dataType:'json',
      processData:false,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var jsonUseData = JSON.parse(jsonData);
        var data = jsonUseData.data;
        $.each(data, function(i,v){
          var gstInfo = {}
          gstInfo['gstName']=v.gstName;
          gstInfo['gstRate']=v.gstRate;
          var jsonData = JSON.stringify(gstInfo);
          var gId = "g"+v.gstId;
          localStorage.setItem(gId,jsonData);
          var print = "<tr><td id="+v.gstId+"Name>"+v.gstName+"</td><td id="+v.gstId+"Rate>"+v.gstRate+"</td><td><a class=btn-floating btn-large waves-effect waves-light href=javascript:updateGst("+v.gstId+")><i class=material-icons>edit</i></a></td></tr>";
          $('#gstInfo').append(print);
        });
      }
    });
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
});

function updateGst(gstId) {
  var gId = "g"+Number(gstId);
  localStorage.setItem("gstUpdateId", gstId);
  if(localStorage.getItem(gId)){
    var gInfo = JSON.parse(localStorage.getItem(gId));
    document.getElementById('gstNameUpdate').value = gInfo.gstName;
    document.getElementById('gstRateUpdate').value = gInfo.gstRate;
    $('#gstUpdate').modal('open');
    $("#gstNameUpdate").focus();
    $("#gstRateUpdate").focus();
  }else{
    window.location.reload();
  }
}

function updateGstFinal() {
  if(localStorage.getItem("gstUpdateId")){
    var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
    var myObj = {};
    var head = sessionInfo.basicAuthenticate;
    myObj['gstId'] = Number(localStorage.getItem("gstUpdateId"));
    myObj['gstName'] = document.getElementById('gstNameUpdate').value;
    myObj['gstRate'] = document.getElementById('gstRateUpdate').value;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/updategstrate',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        Materialize.toast('Please wait for magic', 4000);
        setTimeout(function(){
          location.reload();
        }, 1000);
      },
      error: function (response) {
        var responseJson = JSON.stringify(response);
        var responseJsonR = JSON.parse(responseJson);
        var statusCode = responseJsonR.responseJSON;
        var data = JSON.stringify(statusCode);
        var xData = JSON.parse(data);
        var message = xData.Data;
        Materialize.toast(message, 4000);
      }
    });
  }else{
    window.location.reload();
  }
}

function createGst() {
  if(localStorage.getItem("sessionInfo")){
    $('#gstCreate').modal('open');
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
}

function insertGst() {
  if(localStorage.getItem("sessionInfo")){
    var gstName = document.getElementById('gstName').value;
    var gstRate = document.getElementById('gstRate').value;
    var createObject = {};
    var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
    createObject['gstName']=gstName;
    createObject['gstRate']=gstRate;
    createObject['typeId']=sessionInfo.typeId;
    createObject['userId']=sessionInfo.userId;
    createObject['userType']=sessionInfo.userType;
    var finalJson = JSON.stringify(createObject);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/insertgstrate',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:finalJson,
      success: function(response) {
        Materialize.toast('Please wait for magic', 4000);
        setTimeout(function(){
          location.reload();
        }, 1000);
      },
      error: function (response) {
        var responseJson = JSON.stringify(response);
        var responseJsonR = JSON.parse(responseJson);
        var statusCode = responseJsonR.responseJSON;
        var data = JSON.stringify(statusCode);
        var xData = JSON.parse(data);
        var message = xData.Data;
        Materialize.toast(message, 4000);
      }
    });
  }else{
    window.location.replace(feUrl()+"/index.html");
  }
}
